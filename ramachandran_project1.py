"""Adam is not infected, Roger is infected, Tom is vaccinated"""

print("Please see file ramachandran_project1_spread.py if you want to click through the graphs to watch the virus spread. In that code, the only comments I've made are where you can adjust the total number of people, the number of vaccinated people, and the virusSphere. You can also uncomment the print statements right at the end of this code to see the results as the code runs, before it graphs them.")

import matplotlib.pyplot as plt 
import numpy as np

#Sets up the size of the room

bound_x = 200
bound_y = 200

#establishes a class of people 
class Person(object):

    def __init__(self, x, y, infected = False, vaccinated = False):

        self.x = x
        self.y = y
        self.infected = infected
        self.vaccinated = vaccinated

    def walk (self,bound_x,bound_y):

        #giving my people the ability to walk 

        m = np.random.randint(-5,5)
        n = np.random.randint(-5,5)

        self.x = self.x + m
        self.y = self.y + n

        #making sure they stay in the room
        #this gives each person the motion of a ball bouncing against the edges of a box
        if self.x > bound_x or self.x < 0:
            m = - m
            self.x = self.x + m
            
        if self.y > bound_y or self.y < 0:
            n = - n
            self.y = self.y + n 

    def virus(self):

        self.infected = True

    def vaccinated(self):
        self.vaccinated = True 

    def findAdam (self,adam):

        #if a healthy person is inside the virusSphere, they get infected if they roll a number less than 70.

        virusSphere  = np.sqrt((self.x - adam.x)**2 + (self.y - adam.y)**2)

        #adjust virusSphere here
        
        if virusSphere  <= 20:
            
            if np.random.randint(0,100) < 70:
                adam.virus()

#arrays to store the time and the number of people who are vaccinated. This later gets plotted. 
timePlot = []
vaccinatedPlot = []

#You can change the total number of people here
totalPeople = 100


for p in range (0,totalPeople):
    
    healthyList = []
    infectedList = []
    vaccinatedList = []

    #every round, the number of healthy people decreases by p and the number of vaccinated people increases by p. This models someone healthy getting vaccinated every infection round 

    numHealthy = totalPeople-p
    numInfected = 1
    numVaccinated = p


    # setting up 100 people in the classroom 
    
    for i in range(0,numHealthy):

        x = np.random.randint(0,bound_x)
        y = np.random.randint(0,bound_y)
    
        adam = Person(x,y)

        healthyList.append(adam)

        # initialising the one infected person

    roger = Person(np.random.randint(0,bound_x),np.random.randint(0,bound_y),True)

    infectedList.append(roger)

    # vaccinating the people who need to be vaccinated  

    for z in range(0,numVaccinated):

        x = np.random.randint(0,bound_x)
        y = np.random.randint(0,bound_y)

        tom = Person(x,y)

        vaccinatedList.append(tom)

    time = 0

    #keeps the code running until everyone who can be infected has been

    while len(healthyList) > 0:
    
        x_healthyList = []
        y_healthyList = []

        x_infectedList = []
        y_infectedList = []

        x_vaccinatedList = []
        y_vaccinatedList = []

        InfectedListSize = len(infectedList)
        HealthyListSize = len(healthyList)
    
        for i in range (InfectedListSize):

            j = 0

            while j < HealthyListSize:

                adam = healthyList[j]

                #infects adam 

                infectedList[i].findAdam(adam)

                if healthyList[j].infected == True:

                    #adds this new infected person to the infected list 

                    infectedList.append(healthyList[j])

                    #removes the infected person fro the healthy list 

                    del healthyList[j]

                    HealthyListSize -= 1

                    j = j - 1

                j += 1

        #makes all the people walk 

        for k in range (len(healthyList)):

            healthyList[k].walk(bound_x,bound_y)

            x_healthyList.append(healthyList[k].x)
            y_healthyList.append(healthyList[k].y)

        for k in range (len(infectedList)):

            infectedList[k].walk(bound_x,bound_y)

            x_infectedList.append(infectedList[k].x)
            y_infectedList.append(infectedList[k].y)

        for k in range (len(vaccinatedList)):

            vaccinatedList[k].walk(bound_x,bound_y)

            x_vaccinatedList.append(vaccinatedList[k].x)
            y_vaccinatedList.append(vaccinatedList[k].y)

        
        

        #plt.plot(x_healthyList,y_healthyList,'bo',label = 'healthy people')
        #plt.plot(x_infectedList, y_infectedList, 'ro',label = 'infected people')
        #plt.plot(x_vaccinatedList, y_vaccinatedList, 'co',label = 'vaccinated people')
        #plt.title("The room with infected people, healthy people, and vaccinated people")
        #plt.legend(numpoints=1)
        #plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

        time = time + 1

    #print ("time",time)
    #print("numHealthy",numHealthy)
    #print("numInfected",numInfected)
    #print("numVaccinated",numVaccinated)

    timePlot.append(time)
    vaccinatedPlot.append(numVaccinated)

#print(timePlot)
#print(vaccinatedPlot)

plt.plot(vaccinatedPlot,timePlot,'ro')
plt.xlabel("Number of vaccinated people")
plt.ylabel("Time taken to infect everyone")
plt.title("Number of vaccinated people vs. time taken to infect the whole room")
plt.show()


    
    





        





    




    

    












    