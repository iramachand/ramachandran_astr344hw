import matplotlib.pyplot as plt 
import numpy as np


bound_x = 200
bound_y = 200

class Person(object):

    def __init__(self, x, y, infected = False, vaccinated = False):

        self.x = x
        self.y = y
        self.infected = infected
        self.vaccinated = vaccinated

    def walk (self,bound_x,bound_y):

        m = np.random.randint(-5,5)
        n = np.random.randint(-5,5)

        self.x = self.x + m
        self.y = self.y + n

        if self.x > bound_x or self.x < 0:
            m = - m
            self.x = self.x + m
            
        if self.y > bound_y or self.y < 0:
            n = - n
            self.y = self.y + n 
 

    def virus(self):

        self.infected = True

    def vaccinated(self):
        self.vaccinated = True 

    def findAdam (self,adam):

        virusSphere  = np.sqrt((self.x - adam.x)**2 + (self.y - adam.y)**2)

        #change virusSphere here
        
        if virusSphere  <= 20:
            if np.random.randint(0,100) < 3:
                adam.virus()

            
healthyList = []
infectedList = []
vaccinatedList = []


totalPeople = 100
 

for i in range(0,totalPeople):

    x = np.random.randint(0,bound_x)
    y = np.random.randint(0,bound_y)
    
    adam = Person(x,y)

    healthyList.append(adam)

#change initial number of infected people here 
roger = Person(np.random.randint(0,bound_x),np.random.randint(0,bound_y),True)

infectedList.append(roger)

#change number of vaccinated people here
immune =  Person(np.random.randint(0,bound_x),np.random.randint(0,bound_y))
vaccinatedList.append(immune)

time = 0 

while len(healthyList) > 0:
    
    x_healthyList = []
    y_healthyList = []

    x_infectedList = []
    y_infectedList = []

    x_vaccinatedList = []
    y_vaccinatedList = []

    numInfected = len(infectedList)
    numHealthy = len(healthyList)
    
    for i in range (numInfected):

        j = 0

        while j < numHealthy:

             adam = healthyList[j]

             infectedList[i].findAdam(adam)

             if healthyList[j].infected == True:

                 infectedList.append(healthyList[j])

                 del healthyList[j]

                 numHealthy -= 1

                 j = j - 1

             j += 1 

    for k in range (len(healthyList)):

        healthyList[k].walk(bound_x,bound_y)

        x_healthyList.append(healthyList[k].x)
        y_healthyList.append(healthyList[k].y)

    for k in range (len(infectedList)):

        infectedList[k].walk(bound_x,bound_y)

        x_infectedList.append(infectedList[k].x)
        y_infectedList.append(infectedList[k].y)

    for k in range (len(vaccinatedList)):

        vaccinatedList[k].walk(bound_x,bound_y)

        x_vaccinatedList.append(vaccinatedList[k].x)
        y_vaccinatedList.append(vaccinatedList[k].y)
        

    plt.plot(x_healthyList,y_healthyList,'bo',label = 'healthy people')
    plt.plot(x_infectedList, y_infectedList, 'ro',label = 'infected people')
    plt.plot(x_vaccinatedList, y_vaccinatedList, 'co',label = 'vaccinated people')
    plt.title("The room with infected people, healthy people, and vaccinated people")
    #plt.legend(numpoints=1)
    plt.legend(bbox_to_anchor=(1.05, 1), loc=2, borderaxespad=0.)

    
    plt.show()
   

    time = time + 1

print (time)