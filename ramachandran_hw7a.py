import numpy as np
import math as math

print("To find PI, you have to intergrate a cirlce of radius 1. So the x and y bounds are between 1 and -1. For this assignment, I first looked at just a quater of a circle. I assign random x and y coordinates, and if they fall inside the circle (ie. using pythagoras), it counts as a point inside the circle. Taking points in over the number of points thrown, and multiplying by 4 (for the area of the entire circle), it gives the area.")

def montePI(n):
    

    pointsIN = 0.

    for i in range (0,n):
        
        x = np.random.random()
        y = np.random.random()

        if (math.sqrt(x**2 + y**2)) <= 1:

            pointsIN = pointsIN + 1

        print(pointsIN)

    area = np.float64((pointsIN/n)*4)

    return area
 

print(montePI(100000))
