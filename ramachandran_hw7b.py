import numpy as np

print("------------------------------------------------------------------------")

print("The function monteBirthday takes two inputs, the size and the number of trial runs. Initially, it stores the value of sharedBirthdays = 0. It then cycles through the trails. In each trial, it has an array of birthdays called birthdayList. It also has a match = False boolean. For each student in a class, the next for loop assigns each person a random day of the year as their birthday. It adds this birthday to the birthdayList. The next for loop cycles through each day of the year, and when there are two identical birthdays in birthdayList, it changes our match boolean to true. If there is a match, it increases our initial sharedBirthday by 1. At the end of N trials, the probaility is calculated by dividing the number of sharedBirthdays found by the number of times the loop ran (N).")

def monteBirthday(size,N):
    
    sharedBirthdays = 0.

    for n in range(N):
   
        birthdayList = []
        match = False 

        for i in range(size):

            birthday = np.random.randint(1,365)
            birthdayList.append(birthday)

        for b in range(0,365):
            if birthdayList.count(b) > 1:
                match = True

        if match == True:
            sharedBirthdays += 1

    probability = sharedBirthdays/N
            
    return probability

print("------------------------------------------------------------------------")

print("Just testing whether the monteBirthday function finds the probability, I will manually input a few class size values around the known 23 value:")

print("When the class size is 22, the probability is", monteBirthday(22,1000))
print("When the class size is 23, the probability is", monteBirthday(23,1000))
print("When the class size is 24, the probability is", monteBirthday(24,1000))
print("When the class size is 25, the probability is", monteBirthday(25,1000))
print("As we can see, 23 students is the smallest class size where the probability is more than 0.5")

print("------------------------------------------------------------------------")

print("I am now cycling through different class sizes between 10 and 40, and will print the values at which the probability is between 0.5 and 0.55.")

for size in range (10,40):

    if (monteBirthday(size,1000)) >=0.5:
        if (monteBirthday(size,1000)) <= 0.55:
            print(size)


print("The class size of 23 is once again the first value found between the values of 0.5 and 0.55, so it is the smallest class size that returns a probability above half")

print("------------------------------------------------------------------------")

print("DONE!")